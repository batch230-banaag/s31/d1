// use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http" module let Node.js transfer data using hyper text transfer protocol
    // it can create HTTP server that listents ot server posrts such as....
    //  3000, 4000, 5000, 8000 (usally used for web development)
// The "http module" is a set of individua files that contain a code to create a "component" that helps establish a data transfer between appications
// Clients (devices/browsers) and server (nodeJS/expressJS application) communicate by exchanging individual messages (request/response)

// Request - the mesages sent by the client
// Response - the message sent by the server as response

let http = require("http");

// http method - createServer() we can create an HTTP server that listsens a request on a specified port and gives responses back to the client
// createServer() is a method of the http object 

http.createServer(function (request, response){

    // use writeHead() method to:
    // - set a status code for the response - a 200 means OK
    // - Set the content-type of the reponse - as aplain text message
    // 

    response.writeHead(200, {'Content-Type': 'text/plain'})
    
    // Send the response with the content 'Hello World'
    response.end('Hello World')


}).listen(4000);

// When server is running, console will print the message: 
console.log('Server is running at localhost: 4000')
// use listen(4000) to only display console.log in server terminal
// port is a virtual point where network connections start and end
// a server will be assigned to port 4000 via listen() method
    // where the server will listen to any request that sent to it and will also
    // send response via this port
// the message or ouputs from using console.log will now be displayed in terminal and not on console in browser


